SVG logo of the [AviGNU organization](https://avignu.com/). Original credit to Jean-Christophe Nouveau.

Flyers for the "Forums des Associations" (2017, credit to Stéphane Millot).

There is also `Makefile`, which contains several targets (for the logo only):

* png → SVG to PNG (with **white** background)
* png_transparent → SVG to PNG (with **transparent** background)
* 50 → SVG to PNG (resized to 50%, and **white** background)
* inkscape → SVG to PNG using [inkscape](https://inkscape.org/) (with **transparent** background)
