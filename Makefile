# Convert SVG image to PNG.
#
# It requires ImageMagick's convert and/or inkscape (depends of target).
#

SRC=	logo-avignu.svg

## macros
IM_CMD=	/usr/bin/convert
IM_ARGS=	-density 200
INKSCAPE_CMD=	/usr/bin/inkscape
INKSCAPE_ARGS=	--export-dpi=200 --export-background-opacity=0 \
	--without-gui


all: png

png:
	@$(IM_CMD) $(IM_ARGS) $(SRC) -background white \
		-alpha off $(subst .svg,.png,$(SRC))

png_transparent:
	@$(IM_CMD) $(IM_ARGS) -background transparent \
		$(SRC) $(subst .svg,.png,$(SRC))

inkscape:
	@$(INKSCAPE_CMD) $(INKSCAPE_ARGS) \
		--export-png=$(subst .svg,.png,$(SRC)) \
		$(SRC)

# Target used in adhesion.tex
50: png
	@$(IM_CMD) $(subst .svg,.png,$(SRC)) -resize 50% \
		$(subst .svg,-50.png,$(SRC))

clean:
	@$(RM) $(subst .svg,.png,$(SRC))

.PHONY: inkscape
